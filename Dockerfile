FROM php:8.1-apache

RUN apt-get update && apt-get install -y \
        cron \
        git \
        libzip-dev \
        zip \
        unzip \
        libpq-dev \
        libxml2-dev \
        libwebp-dev \
        libjpeg62-turbo-dev \
        libpng-dev \
        libfreetype6-dev \
        libicu-dev \
        && docker-php-ext-configure gd \
        --with-webp \
        --with-jpeg \
        --with-freetype \
        && docker-php-ext-configure intl \
        && docker-php-ext-install \
        zip \
        bcmath \
        gd \
        pdo_mysql \
        intl \
        pdo_pgsql

WORKDIR /var/www/html
