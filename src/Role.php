<?php

class Role
{
    public static $pdo;

    public static function getAllRoles()
    {
        $roles = self::$pdo->query("SELECT * FROM `roles`")->fetchAll(PDO::FETCH_OBJ);

        return $roles;
    }

    public static function getRoleById($id)
    {
        $stmt = self::$pdo->prepare("SELECT * FROM `roles` WHERE `id` = :id LIMIT 1");
        $stmt->execute(['id' => $id]);
        $role = $stmt->fetch(PDO::FETCH_OBJ);

        return $role;
    }

    public static function addRole($name)
    {
        $stmt = self::$pdo->prepare("INSERT INTO `roles` (`name`) VALUES (:name)");
        $role = $stmt->execute(['name' => $name]);

        return $role;
    }

    public static function updateRoleById($id, $name)
    {
        $stmt = self::$pdo->prepare("UPDATE `roles` SET `name` = :name WHERE `id` = :id");
        $role = $stmt->execute(['id' => $id, 'name' => $name]);

        return $role;
    }

    public static function deleteRoleById($id)
    {
        $stmt = self::$pdo->prepare("DELETE FROM `roles` WHERE `id` = :id");
        $stmt->execute(['id' => $id]);
    }
}
