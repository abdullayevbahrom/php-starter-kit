<?php

class User
{
    public static $pdo;

    public static function getAllUsers()
    {
        $stmt = self::$pdo->query("SELECT * FROM `users`");
        $users = $stmt->fetchAll(PDO::FETCH_OBJ);

        return $users;
    }

    public static function getUserById($id)
    {
        $stmt = self::$pdo->prepare("SELECT * FROM `users` WHERE `id` = :id LIMIT 1");
        $stmt->execute(['id' => $id]);
        $user = $stmt->fetch(PDO::FETCH_OBJ);

        return $user;
    }

    public static function addUser($role_id, $email, $password)
    {
        $stmt = self::$pdo->prepare("INSERT INTO `users` 
            (`role_id`, `email`, `password`) 
            VALUES 
            (:role_id, :email, :password}')");
        $user = $stmt->execute([
            'role_id' => $role_id,
            'email' => $email,
            'password' => $password
        ]);

        return $user;
    }

    public static function updateUserById($id, $role_id, $email, $password)
    {
        $stmt = self::$pdo->prepare(
            "UPDATE `users` SET 
            `role_id` = :role_id,
            `email` = :email,
            `password` = :password
            WHERE `id` = :id"
        );

        $user = $stmt->execute([
            'id' => $id,
            'role_id' => $role_id,
            'email' => $email,
            'password' => $password
        ]);

        return $user;
    }

    public static function deleteUserById($id)
    {
        $stmt = self::$pdo->prepare("DELETE FROM `users` WHERE `id` = :id");
        $stmt->execute(['id' => $id]);
    }
}
