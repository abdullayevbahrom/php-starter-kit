create table
    if not exists roles (
        `id` INT AUTO_INCREMENT PRIMARY KEY,
        `roleName` VARCHAR(50) NOT NULL
    );


insert into roles (`roleName`) values ('admin');

insert into roles (`roleName`) values ('manager');

insert into roles (`roleName`) values ('user');

create table
    if not exists users (
        `id` INT AUTO_INCREMENT PRIMARY KEY,
        `role_id` INT NOT NULL,
        `username` VARCHAR(50) NOT NULL,
        `password` VARCHAR(255) NOT NULL,
        FOREIGN KEY(`role_id`) REFERENCES `roles`(`id`) ON UPDATE CASCADE ON DELETE CASCADE
    );

insert into
    users (`role_id`, `username`, `password`)
values (
        1,
        'admin',
        'adminpassword'
    );

insert into
    users (`role_id`, `username`, `password`)
values (
        2,
        'manager',
        'managerpassword'
    );

insert into
    users (`role_id`, `username`, `password`)
values (
        3,
        'user',
        'userpassword'
    );
